#pragma once

#include <string>
#include <functional>

namespace trace_id {
    std::string trace();
    size_t trace_length();
}