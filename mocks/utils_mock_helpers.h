#pragma once
#include <string>

void set_hostname(const std::string& h);
void set_pid(size_t p);
void set_thread_id(size_t tid);
void set_current_time(size_t t);