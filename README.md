# CUID

C++ port of the Collision-resistant Unique IDentifier spec.
Also includes a few different variations.

Main project site: http://usecuid.org/

**Note:** By default this does not use a cryptographic random number library. If you want a cryptographic random number library, you may provide your own.

## Usage

* `Generator generator;` - Creates a default generator (implements standard, uses mersenne twister)
* `Generator generator{flags};` - Creates a generator with flags (useful for variants)
* `Generator generator{std::function<size_t (void)>};` - Creates a generator with a custom random number generator.
* `Generator generator{std::function<size_t (void)>, flags};` - Creates a generator with a custom random number generator and flags.

`generator();` or `generator.cuid()` - Creates a cuid;

**Note:** By default, the fingerprint will be created when the generator is made and will be cached for all cuid usages.
If you want to override this behavior, use the `cuid::CUID_REGENERATE_FINGERPRINT` flag.

### Variants

* CUID_LONG - This has an 8 character fingerprint instead of a 4 character fingerprint.
* CUID_THREAD - This expands the fingerprint to 8 characters and includes the thread id in the fingerprint. By default, it will use the thread id that the generator was created on - not the thread it was called on. To get the current calling thread, combine it with cuid::CUID_REGENERATE_FINGERPRINT
* CUID_REGENERATE_FINGERPRINT - This will regenerate the fingerprint whenever a cuid is made. Useful if you are including thread ids and using across threads or if you are changing hostnames dynamically.

cuid() returns a short random string with some collision-busting measures.
Safe to use as HTML element ID’s, and unique server-side record lookups.

## Trace ID

Trace id is not meant for collision resistance.
Instead, it's meant to correlate logs for a request and assist in debugging instead of being globally unique.

