#include "../mocks/utils_mock_helpers.h"
#include <cuid/cuid.h>
#include <doctest/doctest.h>

#if CUID_THREAD_SAFE
#include <future>
#include <set>
#endif

TEST_SUITE("CUID standard") {
  TEST_CASE("works") {
    set_pid(9288);
    set_hostname("host");
    set_current_time(19992);
    set_thread_id(1882);

    cuid::Generator generator{cuid::CUID_STANDARD};
    CHECK(generator() == "c00000ffc000060didlt59nk0");
    set_current_time(29992);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "c00000n54000160di5e3rx7a0");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_STANDARD));
  }

  TEST_CASE("works with different settings") {
    set_pid(885568);
    set_hostname("a_different_host");
    set_current_time(1999299348);
    set_thread_id(32);

    cuid::Generator generator{cuid::CUID_STANDARD};
    CHECK(generator() == "c00x2bx900000b4c8mwdq43r7");
    set_current_time(1999299448);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "c00x2bxbs0001b4c8272o341p");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_STANDARD));
  }
}

TEST_SUITE("CUID thread id") {
  TEST_CASE("works") {
    set_pid(9288);
    set_hostname("host");
    set_current_time(19992);
    set_thread_id(1882);

    cuid::Generator generator{cuid::CUID_THREAD};
    CHECK(generator() == "C00000ffc0000ga7600didlt59nk0");
    set_current_time(29992);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "C00000n540001ga7600di5e3rx7a0");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_THREAD));
  }

  TEST_CASE("works with different settings") {
    set_pid(885568);
    set_hostname("a_different_host");
    set_current_time(1999299348);
    set_thread_id(32);

    cuid::Generator generator{cuid::CUID_THREAD};
    CHECK(generator() == "C00x2bx9000000wzb41c8mwdq43r7");
    set_current_time(1999299448);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "C00x2bxbs00010wzb41c8272o341p");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_THREAD));
  }
}

TEST_SUITE("CUID no thread id") {
  TEST_CASE("works") {
    set_pid(9288);
    set_hostname("host");
    set_current_time(19992);

    cuid::Generator generator{cuid::CUID_LONG};
    CHECK(generator() == "C00000ffc0000076000didlt59nk0");
    set_current_time(29992);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "C00000n540001076000di5e3rx7a0");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG));
  }

  TEST_CASE("works with different settings") {
    set_pid(885568);
    set_hostname("a_different_host");
    set_current_time(1999299348);

    cuid::Generator generator{cuid::CUID_LONG};
    CHECK(generator() == "C00x2bx900000izb401c8mwdq43r7");
    set_current_time(1999299448);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "C00x2bxbs0001izb401c8272o341p");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG));
  }
}

TEST_SUITE("CUID long thread id") {
    TEST_CASE("works") {
        set_pid(9288);
set_hostname("host");
set_current_time(19992);

cuid::Generator generator{cuid::CUID_LONG | cuid::CUID_THREAD};
CHECK(generator() == "C00000ffc0000000w076000didlt59nk0");
set_current_time(29992);
set_pid(885569);
set_hostname("different_host");
CHECK(generator() == "C00000n540001000w076000di5e3rx7a0");
CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG | cuid::CUID_THREAD));
}

TEST_CASE("works with different settings") {
  set_pid(885568);
  set_hostname("a_different_host");
  set_current_time(1999299348);

  cuid::Generator generator{cuid::CUID_LONG | cuid::CUID_THREAD};
  CHECK(generator() == "C00x2bx900000000wizb401c8mwdq43r7");
  set_current_time(1999299448);
  set_pid(885569);
  set_hostname("different_host");
  CHECK(generator() == "C00x2bxbs0001000wizb401c8272o341p");
  CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG | cuid::CUID_THREAD));
}

#if CUID_THREAD_SAFE
TEST_CASE("thread safe") {
  set_pid(9288);
  set_hostname("host");
  set_current_time(19992);
  set_thread_id(23);

  cuid::Generator generator{cuid::CUID_LONG | cuid::CUID_THREAD};

  auto f1 = std::async([&generator]() { return generator(); });
  auto f2 = std::async([&generator]() { return generator(); });
  auto f3 = std::async([&generator]() { return generator(); });
  auto f4 = std::async([&generator]() { return generator(); });

  std::set<std::string> vals{};

  vals.emplace(f1.get());
  vals.emplace(f2.get());
  vals.emplace(f3.get());
  vals.emplace(f4.get());

  for(auto& s : vals) {
    CHECK(s.length() == cuid::Generator::expected_length(cuid::CUID_LONG | cuid::CUID_THREAD));
  }

  CHECK(vals.end() != std::find(vals.begin(), vals.end(), "C00000ffc0000000n076000didlt59nk0"));
  CHECK(vals.end() != std::find(vals.begin(), vals.end(), "C00000ffc0001000n076000di5e3rx7a0"));
  CHECK(vals.end() != std::find(vals.begin(), vals.end(), "C00000ffc0002000n076000dia2i3roo1"));
  CHECK(vals.end() != std::find(vals.begin(), vals.end(), "C00000ffc0003000n076000di0d97cx6f"));
}
#endif
}

TEST_SUITE("CUID regenerate") {
  TEST_CASE("works") {
    set_pid(9288);
    set_hostname("host");
    set_current_time(19992);
    set_thread_id(9987);

    cuid::Generator generator{cuid::CUID_THREAD | cuid::CUID_REGENERATE_FINGERPRINT};
    CHECK(generator() == "C00000ffc0000pf7600didlt59nk0");
    set_current_time(29992);
    set_thread_id(84);
    CHECK(generator() == "C00000n5400012c7600di5e3rx7a0");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG));
  }

  TEST_CASE("works with different settings") {
    set_pid(885568);
    set_hostname("a_different_host");
    set_current_time(1999299348);
    set_thread_id(298);

    cuid::Generator generator{cuid::CUID_THREAD | cuid::CUID_REGENERATE_FINGERPRINT};
    CHECK(generator() == "C00x2bx9000008azb41c8mwdq43r7");
    set_current_time(1999299448);
    set_pid(885569);
    set_hostname("different_host");
    CHECK(generator() == "C00x2bxbs00018azb516u272o341p");
    CHECK(generator().length() == cuid::Generator::expected_length(cuid::CUID_LONG));
  }
}

