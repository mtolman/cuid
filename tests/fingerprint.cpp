#include "../mocks/utils_mock_helpers.h"
#include "../src/utils.h"
#include <doctest/doctest.h>

TEST_SUITE("Fingerprint") {
    TEST_CASE("non-standard works") {
        set_pid(1);
        set_hostname("host");

        CHECK(get_fingerprint(cuid::CUID_STANDARD) == "01di");

        set_pid(189);
        CHECK(get_fingerprint(cuid::CUID_STANDARD) == "59di");

        set_hostname("a_new_hope");
        CHECK(get_fingerprint(cuid::CUID_STANDARD) == "59ub");
    }

    TEST_CASE("non-standard works") {
        set_pid(1);
        set_hostname("host");

        CHECK(get_fingerprint(cuid::CUID_LONG) == "000100di");

        set_pid(189);
        CHECK(get_fingerprint(cuid::CUID_LONG) == "005900di");

        set_hostname("a_new_hope");
        CHECK(get_fingerprint(cuid::CUID_LONG) == "005900ub");
    }

    TEST_CASE("with thread works") {
        set_thread_id(1);
        set_pid(189);
        set_hostname("host");

        CHECK(get_fingerprint(cuid::CUID_THREAD) == "010590di");

        set_thread_id(174);
        CHECK(get_fingerprint(cuid::CUID_THREAD) == "4u0590di");
    }
}
