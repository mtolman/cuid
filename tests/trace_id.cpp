#include "../mocks/utils_mock_helpers.h"
#include <cuid/trace_id.h>
#include <doctest/doctest.h>


TEST_SUITE("Trace ID standard") {
    TEST_CASE("works") {
        set_pid(9288);
        set_hostname("host");
        set_current_time(19992);
        set_thread_id(1882);

        CHECK(trace_id::trace() == "t00000ffc01ga076000di");
        CHECK(trace_id::trace() == "t00000ffc01ga076000di");
        set_current_time(29992);
        set_pid(885569);
        set_hostname("different_host");
        set_thread_id(8888);
        CHECK(trace_id::trace() == "t00000n5406uwizb5016u");
        CHECK(trace_id::trace() == "t00000n5406uwizb5016u");
        CHECK(trace_id::trace().length() == trace_id::trace_length());
    }
}
