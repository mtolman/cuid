#pragma once

#include <string>
#include <cuid/flags.h>

std::string get_hostname();
size_t get_pid();
// Defaults to standard cuid behavior
std::string get_fingerprint(unsigned flags = cuid::FLAGS::CUID_STANDARD);
size_t get_thread_id();
std::string to_length(const std::string& str, size_t end_len);
std::string to_base_36(size_t integer);
std::string to_base(size_t integer, size_t base);
size_t get_current_time();
