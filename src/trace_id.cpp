#include <cuid/trace_id.h>
#include "utils.h"

std::string trace_id::trace() {
    auto timestamp1 = to_length(to_base_36(get_current_time()), 8);
    auto fingerprint = get_fingerprint(cuid::FLAGS::CUID_THREAD | cuid::FLAGS::CUID_LONG);
    return std::string("t" + timestamp1 + fingerprint);
}

size_t trace_id::trace_length() {
    return 21;
}
