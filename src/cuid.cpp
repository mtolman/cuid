#include <cuid/cuid.h>
#include "utils.h"
#include <random>
#include <utility>

constexpr size_t max_counter_value = 1679616;
constexpr size_t max_rand_value = max_counter_value * max_counter_value;

struct DefaultRandGenerator {
  std::mt19937_64 generator;

  size_t operator()() {
    return generator();
  }
};

std::string cuid::Generator::cuid() {
  uint32_t count;
  size_t r_num;
  {
#if CUID_THREAD_SAFE
    std::lock_guard<std::mutex> g{rand_mux};
#endif
    count = counter++;
    if (counter >= max_counter_value) {
      counter= 0;
    }
    r_num = rand_num();
  }
  auto rand = to_length(to_base_36(r_num % max_rand_value), 8);
  auto countStr = to_length(to_base_36(count), 4);
  auto timestamp = to_length(to_base_36(get_current_time()), 8);
  auto client_fingerprint = flags & CUID_REGENERATE_FINGERPRINT ? get_fingerprint(flags) : fingerprint;
  if (flags & FLAGS::CUID_STANDARD || !flags) {
    return std::string("c" + timestamp + countStr + client_fingerprint + rand);
  }
  return std::string("C" + timestamp + countStr + client_fingerprint + rand);
}

cuid::Generator::Generator(uint32_t f)
    : flags(f)
      , fingerprint(!(f & CUID_REGENERATE_FINGERPRINT) ? get_fingerprint(f) : "")
      , rand_num(DefaultRandGenerator{std::mt19937_64{get_current_time()}})
{}

cuid::Generator::Generator(std::function<size_t()> rnd, uint32_t f)
    : flags(f)
      , fingerprint(!(f & CUID_REGENERATE_FINGERPRINT) ? get_fingerprint(f) : "")
      , rand_num(std::move(rnd))
{}
